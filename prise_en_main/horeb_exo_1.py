from google.cloud import storage
import os
import datetime
import calendar

def create_bucket(bucket_name):
    # Set the environment variable for authentication
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./Secret/ensai-2024-c9facbbbeae2.json"

    # Create a storage client
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)

    # Create the bucket
    if bucket.exists():
        print(f"Bucket {bucket_name} already exists.")
    else : 
        bucket = storage_client.create_bucket(bucket_name)
        print(f"Bucket {bucket.name} created.")

    

def create_folder_bucket(bucket_name, folder_name) : 
    # Set the environment variable for authentication
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./Secret/ensai-2024-c9facbbbeae2.json"

    # Create a storage client
    storage_client = storage.Client()

    # Create a bucket
    blobs = storage_client.list_blobs(bucket_name, prefix=folder_name)
    if any(blobs) : 
        print(f"Folder {folder_name} already exists in bucket {bucket_name}.")
    else : 
        blob = storage_client.bucket(bucket_name).blob(folder_name)
        blob.upload_from_string("") # Upload an empty string to create a folder
        print(f"Folder {folder_name} created in bucket {bucket_name}.")


if __name__ == "__main__":
    # Create a bucket
    bucket_name = "horeb_bucket"
    create_bucket(bucket_name)

    # #Iterate through date of year 2024 to create a file named by the date 
    # date = datetime.date(2024, 1, 1)

    # while date < datetime.date(2025,1,1) : 
    #     folder_name = date.strftime("%Y-%m-%d")+"/"
    #     create_folder_bucket(bucket_name, folder_name)
    #     date += datetime.timedelta(days=1)

    # # Gather the folders in folders named by the month
    # ## Create a folder for each month in the bucket
    # for month_idx in range(1,13) : 
    #     month_name = calendar.month_name[month_idx]
    #     folder_name = month_name+"-2024/"
    #     create_folder_bucket(bucket_name, folder_name)
    
    # ## Get the folder of each month in the bucket
    # for month_idx in range(1,13) : 
    #     month_name = calendar.month_name[month_idx]
    #     folder_name = month_name+"-2024/"
    #     create_folder_bucket(bucket_name, folder_name)


    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./Secret/ensai-2024-c9facbbbeae2.json"
    # storage_client = storage.Client()

    # blobs = storage_client.list_blobs(bucket_name)
    # for blob in blobs : 
    #     if blob.name.startswith("2024-") and blob.name.endswith("/"):
    #         date_str = blob.name.rstrip("/")
    #         #print(date_str)
    #         date = datetime.datetime.strptime(date_str, "%Y-%m-%d")
    #         #print(date)
    #         month_folder = date.strftime("%B")
    #         destination_blob_name = month_folder + "-2024/" + date_str + "/"
    #         # Copy the blob to the new location
    #         storage_client.bucket(bucket_name).copy_blob(blob, storage_client.bucket(bucket_name), destination_blob_name)

    #         # Delete the original blob (optional, depending on your use case)
    #         blob.delete()
    #         print(f"Blob {blob.name} copied to {destination_blob_name}.")

    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./Secret/ensai-2024-c9facbbbeae2.json"
    # storage_client = storage.Client()
    # blobs = storage_client.list_blobs(bucket_name)
    # for blob in blobs:
    #     if blob.name.endswith("/"):
    #         blob.delete()
    # print("All folders deleted.")
