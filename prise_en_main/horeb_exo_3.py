from dotenv import load_dotenv
import os
import pandas as pd
from sqlalchemy import create_engine

load_dotenv()
db_user = os.getenv('DB_USER')
db_password = os.getenv('DB_PASSWORD')
db_host = os.getenv('DB_HOST')
db_port = os.getenv('DB_PORT')
db_name = os.getenv('DB_NAME')
table_name = os.getenv('TABLE_NAME')


# Import the csv file
df = pd.read_csv(os.getenv('CSV_FILE_PATH'))

# Create a connexion to the database
engine = create_engine(f"postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}")

# Insert the data into the database
df.to_sql(table_name, engine, if_exists='replace', index=False)

print(f"Les données ont été chargées dans la table {table_name}.")
