import pyarrow.parquet as pq
import numpy as np
import pandas as pd
import pyarrow as pa

df = pd.DataFrame({"name" : ["apple", 'pear', 'banana'], 
                   "price" : [2,3,1]})

table = pa.Table.from_pandas(df)

pq.write_table(table, "test.parquet")