import pandas as pd
import urllib.request, json

def load_data(url, datatype = "json"):
    if datatype == "json":
        with urllib.request.urlopen(url) as url:
            data = json.loads(url.read().decode())
            print("Downloading data ....")
            data = pd.DataFrame(data)
    elif datatype == "csv":
        data = pd.read_csv(url)
        print("Downloading data ....")
    return data



if __name__ == '__main__':
    dict_data_json = {"annual_consumption" : "https://www.data.gouv.fr/fr/datasets/r/6fa2126d-4c46-40f5-8a52-d5f3eae92dce",
                "annual_electric_prod" : "https://www.data.gouv.fr/fr/datasets/r/c82ea77e-9e49-4806-9fbd-f96d65bb16b9",
                  }
    dict_data_csv = {
        "change_residential_electricity" : "https://www.data.gouv.fr/fr/datasets/r/c13d05e5-9e55-4d03-bf7e-042a2ade7e49"
    }